﻿
using System;
using System.Collections;
using System.Collections.Generic;



namespace jayde.Collection
{
    public partial class DictionaryObserver<TKey, TValue> : 
        System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey, TValue>>, 
        System.Collections.Generic.IDictionary<TKey, TValue>, 
        System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey, TValue>>, 
        System.Collections.Generic.IReadOnlyCollection<System.Collections.Generic.KeyValuePair<TKey, TValue>>, 
        System.Collections.Generic.IReadOnlyDictionary<TKey, TValue>
        // if functionality from these are needed. It might be better to just get the core interface dictionary version
        // also we are interfacing with IDicitionary<T,T> in generics, not IDictionary in Collections
        //System.Collections.IDictionary, 
        //System.Runtime.Serialization.IDeserializationCallback, 
        //System.Runtime.Serialization.ISerializable
    {


        private readonly IDictionary<TKey,TValue> _dictionary;
        private NotifyUpdateInformation<TKey, TValue> _notifyUpdate = new NotifyUpdateInformation<TKey, TValue>(); //we could pool this for threading
        private readonly Dictionary<TKey, List<Action<object, NotifyUpdateInformation<TKey, TValue>>>> _eventHandlers = new Dictionary<TKey, List<Action<object, NotifyUpdateInformation<TKey, TValue>>>>();
        public bool RemoveEventHandlerIfEmpty { get; set; } = true;


        public IDictionary<TKey, TValue> ImplementedDictionary => _dictionary;

        public int Count => _dictionary.Count;

        public bool IsReadOnly => _dictionary.IsReadOnly;

        public ICollection<TKey> Keys => _dictionary.Keys;

        public ICollection<TValue> Values => _dictionary.Values;

        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => _dictionary.Keys;

        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => _dictionary.Values;

        //public delegate void NotificationEvent(object sender, NotifyUpdateInformation<TKey, TValue> notifyInfo);
        public event EventHandler<NotifyUpdateInformation<TKey, TValue>> allNotifiction;
    



        public DictionaryObserver(IDictionary<TKey,TValue> dictionary)
        {
            _dictionary = dictionary;
        }


        public TValue this[TKey key] { 
            get => _dictionary[key]; 
            set => Update(key, value); 
        }

        private void Update(TKey key, TValue value )
        {

            var lastValue = _dictionary.ContainsKey(key) ? _dictionary[key] : default;
            _dictionary[key] = value;
            Notify(key, value, lastValue, NotifyChangeReason.Update);
        }


        private void Notify(TKey key, TValue value, TValue previous, NotifyChangeReason reason)
        {
            _notifyUpdate.Key = key;
            _notifyUpdate.Value = value;
            _notifyUpdate.PreviousValue = previous;
            _notifyUpdate.ChangeReason = reason;

            allNotifiction?.Invoke(this, _notifyUpdate);

            if (_eventHandlers.ContainsKey(key))
            {
                _eventHandlers[key].ForEach( action =>                
                    action?.Invoke(this, _notifyUpdate));
                
            }
        }


        public void Observe(TKey key, Action<object, NotifyUpdateInformation<TKey, TValue>> eventAction)
        {
            if( _eventHandlers.TryGetValue(key, out var handlerList) == false)
            {
                handlerList = new List<Action<object, NotifyUpdateInformation<TKey, TValue>>>();
                _eventHandlers.Add(key, handlerList);
            }
            
            handlerList.Add(eventAction);
        }

        public void UnObserver(TKey key, Action<object, NotifyUpdateInformation<TKey, TValue>> eventAction)
        {
            if (_eventHandlers.TryGetValue(key, out var handlerList) == false)
            {
                // there is no list for the event actiont to remove
                return;
            }

            handlerList.Remove(eventAction);

            if( handlerList.Count <= 0 && RemoveEventHandlerIfEmpty)
            {
                _eventHandlers.Remove(key);
            }
        }

        public void Clear()
        {
            foreach(var key in _dictionary.Keys)
            {
                //Remove(key);
                Notify(key, _dictionary[key], _dictionary[key], NotifyChangeReason.Removed);
            }
            _dictionary.Clear();
            _eventHandlers.Clear();
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            _dictionary.Add(item);
            Notify(item.Key, item.Value, default, NotifyChangeReason.Added);
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _dictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _dictionary.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            var removed = _dictionary.Remove(item);
            Notify(item.Key, item.Value, default, NotifyChangeReason.Removed);
            return removed;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        public void Add(TKey key, TValue value)
        {
            _dictionary.Add(key, value);
            Notify(key, value, default, NotifyChangeReason.Added);
        }

        public bool ContainsKey(TKey key)
        {
            return _dictionary.ContainsKey(key);
        }

        public bool Remove(TKey key)
        {
            var value = _dictionary[key];
            var removed = _dictionary.Remove(key);
            Notify(key, value, value, NotifyChangeReason.Removed);

            return removed;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _dictionary.TryGetValue(key, out value);
        }






    }




    public class NotifyUpdateInformation<TKey, TValue>
    {
        public TKey Key;
        public TValue Value;
        public TValue PreviousValue;
        public NotifyChangeReason ChangeReason;
    }


    public enum NotifyChangeReason
    {
        None = 0,
        Added = 1,
        Update = 2,
        Removed = 3
    }




}
